import 'dart:async';

import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/libs/CategoryProvider.dart';
import 'package:mgh/models/Category.dart';

class CategoryStore extends Store {
  final _category = Category();
  final _provider = CategoryProvider();

  CategoryStore() {
    triggerOnAction(loadCategoryFromServer, (nothing) async {
      Stream<Category> stream = await _category.getFromServer();
      if (stream == null) {
        print("Imposible cargar Billboards");
        return;
      }

      if (store_categories.isEmpty == false) {
        store_categories.clear();
      }

      List<Category> _categories = await stream.toList();
      for (Category category in _categories) {
        store_categories.add(category);
      }
      List<Category> currents = await _provider.all();
      for (Category category in currents) {
        await _provider.delete(category);
      }
      for (Category category in categories) {
        await _provider.insert(category);
      }
    });

    triggerOnAction(loadCategoryFromSql, (nothing) async {
      List<Category> _categories = await _provider.all();
      for (Category category in _categories) {
        store_categories.add(category);
      }
    });

    triggerOnAction(findCategoryById, (int category_id) async {
      Category _category = await _provider.find(category_id);
      store_category = _category;
    });
  }

  final List<Category> store_categories = <Category>[];
  Category store_category = Category();

  List<Category> get categories =>
      List<Category>.unmodifiable(store_categories);

  Category get category => store_category;
}

final Action loadCategoryFromServer = Action();
final Action loadCategoryFromSql = Action();
final Action<int> findCategoryById = Action<int>();

final StoreToken categoryStoreToken = StoreToken(CategoryStore());
