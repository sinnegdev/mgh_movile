import 'dart:async';

import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/libs/AttributeProvider.dart';
import 'package:mgh/models/Attribute.dart';

class AttributeStore extends Store {
  final _attribute = Attribute();
  final _provider = AttributeProvider();

  AttributeStore() {
    triggerOnAction(loadAttributeFromServer, (nothing) async {
      Stream<Attribute> stream = await _attribute.getFromServer();
      if (stream == null) {
        return;
      }

      List<Attribute> _products = await stream.toList();
      List<Attribute> currents = await _provider.all();
      for (Attribute product in currents) {
        await _provider.delete(product.id);
      }
      for (Attribute product in _products) {
        await _provider.insert(product);
      }
    });

    triggerOnAction(findAttributeByProductId, (int product_id) async {
      List<Attribute> _attributes = await _provider.get(product_id);
      store_attributes.clear();
      for (Attribute product in _attributes) {
        store_attributes.add(product);
      }
    });
  }

  final List<Attribute> store_attributes = <Attribute>[];
  List<Attribute> get attributes =>
      List<Attribute>.unmodifiable(store_attributes);
}

final Action loadAttributeFromServer = Action();
final Action<int> findAttributeByProductId = Action<int>();

final StoreToken attributeStoreToken = StoreToken(AttributeStore());
