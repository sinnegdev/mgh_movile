import 'dart:async';

import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/libs/BillboardProvider.dart';
import 'package:mgh/models/Billboard.dart';

class BillboardStore extends Store {
  final _billboard = Billboard();
  final _provider = BillboardProvider();

  BillboardStore() {
    triggerOnAction(loadBillboardFromServer, (nothing) async {
      Stream<Billboard> stream = await _billboard.getFromServer();
      if (stream == null) {
        print("Imposible cargar Billboards");
        return;
      }

      if (store_billboards.isEmpty == false) {
        store_billboards.clear();
      }

      List<Billboard> _billboards = await stream.toList();
      List<Billboard> currents    = await _provider.all();
      for (Billboard billboard in _billboards) {
        store_billboards.add(billboard);
      }
      for (Billboard billboard in currents) {
        await _provider.delete(billboard);
      }
      for (Billboard billboard in billboards) {
        await _provider.insert(billboard);
      }
    });

    triggerOnAction(loadBillboardFromSql, (nothing) async {
      List<Billboard> _billboards = await _provider.all();
      for (Billboard billboard in _billboards) {
        store_billboards.add(billboard);
      }
    });
  }

  final List<Billboard> store_billboards = <Billboard>[];
  final Billboard store_billboard = Billboard();

  List<Billboard> get billboards =>
      List<Billboard>.unmodifiable(store_billboards);

  Billboard get billboard => store_billboard;
}

final Action loadBillboardFromServer = Action();
final Action loadBillboardFromSql = Action();

final StoreToken billboardStoreToken = StoreToken(BillboardStore());
