import 'dart:async';

import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/libs/ProductProvider.dart';
import 'package:mgh/models/Product.dart';

class ProductStore extends Store {
  final _product = Product();
  final _provider = ProductProvider();

  ProductStore() {
    triggerOnAction(loadProductFromServer, (nothing) async {
      Stream<Product> stream = await _product.getFromServer();
      if (stream == null) {
        return;
      }

      List<Product> _products = await stream.toList();
      List<Product> currents = await _provider.all();
      for (Product product in currents) {
        await _provider.delete(product.id);
      }
      for (Product product in _products) {
        await _provider.insert(product);
      }
    });

    triggerOnAction(loadProductFromSql, (nothing) async {
      List<Product> _products = await _provider.all();
      store_products.clear();
      for (Product product in _products) {
        store_products.add(product);
      }
    });

    triggerOnAction(findProductById, (int product_id) async {
      Product _product = await _provider.select(product_id);
      store_product = _product;
    });

    triggerOnAction(findProductByCategory, (int category_id) async {
      List<Product> _products = await _provider.select_by_category(category_id);
      store_products.clear();
      for (Product product in _products) {
        store_products.add(product);
      }
    });
  }

  final List<Product> store_products = <Product>[];
  Product store_product;

  List<Product> get products => List<Product>.unmodifiable(store_products);
  Product get product => store_product;
}

final Action loadProductFromServer = Action();
final Action loadProductFromSql = Action();
final Action<int> findProductById = Action<int>();
final Action<int> findProductByCategory = Action<int>();

final StoreToken productStoreToken = StoreToken(ProductStore());
