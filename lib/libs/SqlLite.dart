import 'dart:async';

import 'package:meta/meta.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

abstract class SqlLite {
  final String database = "mgh.db";
  @protected
  Database db;
  @protected
  var table;
  @protected
  var fields;

  Future<Database> prepare() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, database);
    db = await openDatabase(
      path,
      version: 1,
      onOpen: (Database db) async {
        print("Creando Banners");
        await db.execute('''
          CREATE TABLE IF NOT EXISTS banners (
            id          integer primary key autoincrement,
            picture     text
          )
        ''');
        print("Creando Products");
        await db.execute('''
          CREATE TABLE IF NOT EXISTS products (
            id              integer primary key autoincrement,
            category_id     integer,
            picture         text,
            product         text,
            description     text
          )
         ''');
        print("Creando Categories");
        await db.execute('''
          CREATE TABLE IF NOT EXISTS categories (
            id          integer primary key autoincrement,
            category    text,
            color       text,
            picture     text,
            banner      text,
            columns     integer,
            rows        integer
          )
        ''');
        print("Creando Attributes");
        await db.execute('''
          CREATE TABLE IF NOT EXISTS attributes (
            id          integer primary key autoincrement,
            title       text,
            description text,
            product_id  integer
          )
        ''');
      },
    );
    return db;
  }

  Future close() async => db.close();
}
