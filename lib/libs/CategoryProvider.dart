import 'dart:async';

import 'package:mgh/libs/SqlLite.dart';
import 'package:mgh/models/Category.dart';
import 'package:sqflite/sqflite.dart';

class CategoryProvider extends SqlLite {
  @override
  var table = "categories";
  @override
  var fields = [
    "id",
    "category",
    "color",
    "picture",
    "banner",
    "columns",
    "rows"
  ];
  @override
  Database db;

  openConnection() async {
    if (db == null) await prepare();
  }

  Future<Category> insert(Category billboard) async {
    await openConnection();
    billboard.id = await db.insert(table, billboard.toMap());
    return billboard;
  }

  Future<int> update(Category billboard) async {
    await openConnection();
    var result = await db.update(table, billboard.toMap(),
        where: "id = ?", whereArgs: [billboard.id]);
    return result;
  }

  Future<int> delete(Category billboard) async {
    await openConnection();
    var result =
        await db.delete(table, where: "id = ?", whereArgs: [billboard.id]);
    return result;
  }

  Future<Category> find(int id) async {
    await openConnection();
    List<Map> maps = await db
        .query(table, columns: fields, where: "id = ?", whereArgs: [id]);
    if (maps.length > 0) return new Category.fromMap(maps.first);
    return null;
  }

  Future<List<Category>> all() async {
    await openConnection();
    List<Map> maps = await db.query(table);
    if (maps.length > 0) return Category.fromMaps(maps);
    return <Category>[];
  }
}
