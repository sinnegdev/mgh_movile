import 'dart:async';

import 'package:mgh/libs/SqlLite.dart';
import 'package:mgh/models/Billboard.dart';
import 'package:sqflite/sqflite.dart';

class BillboardProvider extends SqlLite {
  @override var table  = "banners";
  @override var fields = ["id", "picture"];
  @override Database db = null;

  openConnection() async{
    if(db == null)
      await prepare();
  }

  Future<Billboard> insert(Billboard billboard) async {
    await openConnection();
    billboard.id = await db.insert(table, billboard.toMap());
    return billboard;
  }

  Future<int> update(Billboard billboard) async {
    await openConnection();
    var result = await db.update(table, billboard.toMap(), where: "id = ?", whereArgs: [billboard.id]);
    return result;
  }

  Future<int> delete(Billboard billboard) async {
    await openConnection();
    var result = await db.delete(table, where: "id = ?", whereArgs: [billboard.id]);
    return result;
  }

  Future<Billboard> select(int id) async {
    await openConnection();
    List<Map> maps = await db.query(
        table,
        columns: fields,
        where: "id = ?",
        whereArgs: [id]
    );
    if(maps.length > 0)
      return new Billboard.fromMap(maps.first);
    return null;
  }

  Future<List<Billboard>> all() async {
    await openConnection();
    List<Map> maps = await db.query(table);
    if(maps.length > 0)
      return Billboard.fromMaps(maps);
    return <Billboard>[];
  }


}