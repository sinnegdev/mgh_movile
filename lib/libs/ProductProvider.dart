import 'dart:async';

import 'package:mgh/libs/SqlLite.dart';
import 'package:mgh/models/Product.dart';
import 'package:sqflite/sqflite.dart';

class ProductProvider extends SqlLite {
  @override
  var table = "products";

  @override
  var fields = ["id", "category_id", "product", "picture", "description"];

  @override
  Database db;

  openConnection() async {
    if (db == null) await prepare();
  }

  Future<Product> insert(Product product) async {
    await openConnection();
    product.id = await db.insert(table, product.toMap());
    return product;
  }

  Future<int> update(Product product) async {
    await openConnection();
    var result = await db.update(table, product.toMap(),
        where: "id = ?", whereArgs: [product.id]);
    return result;
  }

  Future<int> delete(int id) async {
    await openConnection();
    var result = await db.delete(table, where: "id = ?", whereArgs: [id]);
    return result;
  }

  Future<Product> select(int id) async {
    await openConnection();
    List<Map> maps = await db.query(table, where: '"id" = ?', whereArgs: [id]);
    return Product.fromMap(maps.first);
  }

  Future<List<Product>> select_by_category(int categoryId) async {
    await openConnection();
    List<Map> maps = await db
        .query(table, where: 'category_id = ?', whereArgs: [categoryId]);
    if (maps.length > 0) return Product.fromMaps(maps);
    return <Product>[];
  }

  Future<List<Product>> all() async {
    await openConnection();
    List<Map> maps = await db.query(table);
    if (maps.length > 0) return Product.fromMaps(maps);
    return <Product>[];
  }
}
