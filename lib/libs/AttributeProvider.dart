import 'dart:async';

import 'package:mgh/libs/SqlLite.dart';
import 'package:mgh/models/Attribute.dart';
import 'package:sqflite/sqflite.dart';

class AttributeProvider extends SqlLite {
  @override
  var table = "attributes";

  @override
  var fields = ["id", "title", "description", "product_id"];

  @override
  Database db;

  openConnection() async {
    if (db == null) await prepare();
  }

  Future<Attribute> insert(Attribute attribute) async {
    await openConnection();
    attribute.id = await db.insert(table, attribute.toMap());
    return attribute;
  }

  Future<int> update(Attribute attribute) async {
    await openConnection();
    var result = await db.update(table, attribute.toMap(),
        where: "id = ?", whereArgs: [attribute.id]);
    return result;
  }

  Future<int> delete(int id) async {
    await openConnection();
    var result = await db.delete(table, where: "id = ?", whereArgs: [id]);
    return result;
  }

  Future<List<Attribute>> get(int productId) async {
    await openConnection();
    List<Map> maps =
        await db.query(table, where: 'product_id = ?', whereArgs: [productId]);
    if (maps.length > 0) return Attribute.fromMaps(maps);
    return <Attribute>[];
  }

  Future<List<Attribute>> all() async {
    await openConnection();
    List<Map> maps = await db.query(table);
    if (maps.length > 0) return Attribute.fromMaps(maps);
    return <Attribute>[];
  }
}
