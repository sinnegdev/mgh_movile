import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Category {
  int id;
  String category;
  String color;
  String picture;
  String banner;
  int columns;
  int rows;
  Color get hexColor => new Color(
      int.parse(color.toString().substring(1, 7), radix: 16) + 0xFF000000);

  Category({
    this.id,
    this.category,
    this.color,
    this.picture,
    this.banner,
    this.columns,
    this.rows,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "picture": picture,
      "banner": banner,
      "color": color,
      "rows": rows,
      "columns": columns,
      "category": category,
    };

    if (id != null) {
      map["id"] = id;
    }

    return map;
  }

  factory Category.fromJson(Map<String, dynamic> json) {
    return new Category(
      id: json['id'] as int,
      category: json['category'] as String,
      color: json['color'] as String,
      picture: json['picture'] as String,
      banner: json['banner'] as String,
      columns: json['columns'] as int,
      rows: json['rows'] as int,
    );
  }

  static List<Category> fromMaps(List<Map> maps) {
    return maps.map<Category>((json) => new Category.fromJson(json)).toList();
  }

  Category.fromMap(Map<String, dynamic> map) {
    id = map['id'] as int;
    category = map['category'] as String;
    color = map['color'] as String;
    picture = map['picture'] as String;
    banner = map['banner'] as String;
    columns = map['columns'] as int;
    rows = map['rows'] as int;
  }

  static List<Category> parse(String jsonUnparsed) {
    final parsed = json.decode(jsonUnparsed).cast<Map<String, dynamic>>();
    return parsed.map<Category>((json) => new Category.fromJson(json)).toList();
  }

  Future<Stream<Category>> getFromServer() async {
    String url = "http://mgh.sinneg.com/api/categories";
    Client client = http.Client();
    StreamedResponse streamedRes;
    try {
      streamedRes = await client.send(http.Request('get', Uri.parse(url)));
    } catch (e) {
      return null;
    }
    return streamedRes.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .expand((body) => (body as List))
        .map((json) => Category.fromJson(json));
  }
}
