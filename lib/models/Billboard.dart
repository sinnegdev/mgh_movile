import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Billboard {
  int id;
  String picture;

  Billboard({
    this.id,
    this.picture,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "picture": picture,
    };
    if (id != null) {
      map["id"] = id;
    }
    return map;
  }

  factory Billboard.fromJson(Map<String, dynamic> json) {
    return new Billboard(
      id: json['id'] as int,
      picture: json['picture'] as String,
    );
  }

  static List<Billboard> fromMaps(List<Map> maps) {
    return maps.map<Billboard>((json) => new Billboard.fromJson(json)).toList();
  }

  Billboard.fromMap(Map<String, dynamic> map) {
    id = map['id'] as int;
    picture = map['picture'] as String;
  }

  static List<Billboard> parse(String jsonUnparsed) {
    final parsed = json.decode(jsonUnparsed).cast<Map<String, dynamic>>();
    return parsed
        .map<Billboard>((json) => new Billboard.fromJson(json))
        .toList();
  }

  Future<Stream<Billboard>> getFromServer() async {
    String url = "http://mgh.sinneg.com/api/banners";
    Client client = http.Client();
    StreamedResponse streamedRes;
    try {
      streamedRes = await client.send(http.Request('get', Uri.parse(url)));
    } catch (e) {
      return null;
    }
    return streamedRes.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .expand((body) => (body as List))
        .map((json) => Billboard.fromJson(json));
  }
}
