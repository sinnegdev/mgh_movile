import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Attribute {
  final String table = "attributes";

  int id;
  String title;
  String description;
  int product_id;

  Attribute({
    this.id,
    this.title,
    this.description,
    this.product_id,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "title": title,
      "description": description,
      "product_id": product_id,
    };
    if (id != null) {
      map["id"] = id;
    }
    return map;
  }

  factory Attribute.fromJson(Map<String, dynamic> json) {
    return new Attribute(
        id: json['id'] as int,
        title: json["title"] as String,
        description: json["description"] as String,
        product_id: json["product_id"] as int);
  }

  static List<Attribute> fromMaps(List<Map> maps) {
    return maps.map<Attribute>((json) => new Attribute.fromJson(json)).toList();
  }

  Attribute.fromMap(Map<String, dynamic> map) {
    id = map['id'] as int;
    title = map['title'] as String;
    description = map['description'] as String;
    product_id = map['product_id'] as int;
  }

  static List<Attribute> parse(String jsonUnparsed) {
    final parsed = json.decode(jsonUnparsed).cast<Map<String, dynamic>>();
    return parsed
        .map<Attribute>((json) => new Attribute.fromJson(json))
        .toList();
  }

  Future<Stream<Attribute>> getFromServer() async {
    String url = "http://mgh.sinneg.com/api/attributes";
    Client client = http.Client();
    StreamedResponse streamedRes;
    try {
      streamedRes = await client.send(http.Request('get', Uri.parse(url)));
    } catch (e) {
      return null;
    }
    return streamedRes.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .expand((body) => (body as List))
        .map((json) => Attribute.fromJson(json));
  }
}
