import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Product {
  int id;
  int category_id;
  String product;
  String picture;
  String description;

  Product(
      {this.id,
      this.category_id,
      this.product,
      this.picture,
      this.description});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};

    if (id != null) map["id"] = id;
    if (picture != null) map["picture"] = picture;
    if (category_id != null) map["category_id"] = category_id;
    if (product != null) map["product"] = product;
    if (description != null) map["description"] = description;

    return map;
  }

  factory Product.fromJson(Map<String, dynamic> json) {
    return new Product(
      id: json["id"] as int,
      category_id: json["category_id"] as int,
      product: json["product"] as String,
      picture: json["picture"] as String,
      description: json["description"] as String,
    );
  }

  static List<Product> fromMaps(List<Map> maps) {
    return maps.map<Product>((json) => new Product.fromJson(json)).toList();
  }

  Product.fromMap(Map<String, dynamic> map) {
    id = map["id"] as int;
    category_id = map["category_id"] as int;
    product = map["product"] as String;
    picture = map["picture"] as String;
    description = map["description"] as String;
  }

  static List<Product> parse(String jsonUnparsed) {
    final parsed = json.decode(jsonUnparsed).cast<Map<String, dynamic>>();
    return parsed.map<Product>((json) => new Product.fromJson(json)).toList();
  }

  Future<Stream<Product>> getFromServer() async {
    String url = "http://mgh.sinneg.com/api/products";
    Client client = http.Client();
    StreamedResponse streamedRes;
    try {
      streamedRes = await client.send(http.Request("get", Uri.parse(url)));
    } catch (e) {
      return null;
    }
    return streamedRes.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .expand((body) => (body as List))
        .map((json) => Product.fromJson(json));
  }
}
