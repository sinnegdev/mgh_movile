import 'package:flutter/material.dart';
import 'package:mgh/components/TitleBox.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        child: new Padding(
          padding: EdgeInsets.symmetric(vertical: 25.0, horizontal: 15.0),
          child: new Column(
            children: <Widget>[
              new Padding(padding: EdgeInsets.all(45.0),
              child: new Image.asset("images/logo.png"),),
              new TitleBox(title: "¿QUIÉNES SOMOS?",textAlign: TextAlign.left),
              new Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
              new TitleBox(fontSize: 18.0, title: "Somos una empresa mexicana que comercializa productos nutracéuticos con los más altos estándares de calidad, hechos con ingredientes de origen 100% natural y apegados estrictamente la regulación sanitaria vigente."),
              new Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
              new TitleBox(fontSize: 16.0, title: "• Buscamos la salud de las familias y su bienestar económico a través de un modelo de negocio de network marketing, rentable, justo y equitativo."),
              new Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
              new TitleBox(fontSize: 16.0, title: "• Estamos comprometidos con nuestros consumidores para ofrecerles productos de calidad, con nuestros distribuidores y proveedores a mantener relaciones de negocio ganar-ganar y con nuestros colaboradores a un desarrollo profesional y una remuneración competitiva en el mercado."),
              new Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
              new TitleBox(fontSize: 16.0, title: "• Somos Mega Health, 20 años nutriendo tus ganas de triunfar."),
            ],
          ),
        ),
      ),
    );
  }
}
