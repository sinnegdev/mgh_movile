import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/components/TitleBox.dart';
import 'package:mgh/stores/AttributeStore.dart';
import 'package:mgh/stores/BillboardStore.dart';
import 'package:mgh/stores/CategoryStore.dart';
import 'package:mgh/stores/ProductStore.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>
    with StoreWatcherMixin<SplashScreen> {
  BillboardStore billboardStore;
  CategoryStore categoryStore;
  ProductStore productStore;
  AttributeStore attributeStore;

  goToHomeScreen() {
    Navigator.of(context).pushReplacementNamed('HomeScreen');
  }

  fetchFromServer() async {
    print("Cargando datos remotamente");
    await loadBillboardFromServer.call();
    await loadCategoryFromServer.call();
    await loadProductFromServer.call();
    await loadAttributeFromServer.call();
  }

  fetchFromLocal() async {
    print("Cargando datos localmente");
    await loadBillboardFromSql.call();
    await loadCategoryFromSql.call();
  }

  fetchData() async {
    try {
      var connectivityResult = (await new Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        fetchFromLocal();
      } else {
        fetchFromServer();
      }
    } on PlatformException catch (e) {
      fetchFromLocal();
    }
  }

  @override
  void initState() {
    super.initState();
    billboardStore = listenToStore(billboardStoreToken);
    categoryStore = listenToStore(categoryStoreToken);
    productStore = listenToStore(productStoreToken);
    attributeStore = listenToStore(attributeStoreToken);
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new GestureDetector(
      child: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: new AssetImage("images/splashscreen.png"),
            fit: BoxFit.cover,
            color: Colors.white10,
            colorBlendMode: BlendMode.lighten,
          ),
          new Container(
            margin: EdgeInsets.symmetric(vertical: 30.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: new AssetImage("images/logo.png"),
                  width: 200.0,
                ),
                new Padding(padding: new EdgeInsets.all(15.0)),
                new TitleBox(title: 'PREVENCIÓN ES SALUD'),
              ],
            ),
          )
        ],
      ),
      onTap: () {
        goToHomeScreen();
      },
    ));
  }
}
