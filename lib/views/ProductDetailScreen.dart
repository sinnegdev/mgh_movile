import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/components/TitleBox.dart';
import 'package:mgh/models/Attribute.dart';
import 'package:mgh/models/Category.dart';
import 'package:mgh/stores/AttributeStore.dart';
import 'package:mgh/stores/CategoryStore.dart';
import 'package:mgh/stores/ProductStore.dart';
import 'package:mgh/views/AboutScreen.dart';

class ProductDetailScreen extends StatefulWidget {
  @override
  ProductDetailScreenState createState() => new ProductDetailScreenState();
}

class ProductDetailScreenState extends State<ProductDetailScreen>
    with StoreWatcherMixin<ProductDetailScreen> {
  ProductStore productStore;
  CategoryStore categoryStore;
  AttributeStore attributeStore;

  @override
  void initState() {
    super.initState();
    productStore = listenToStore(productStoreToken);
    categoryStore = listenToStore(categoryStoreToken);
    attributeStore = listenToStore(attributeStoreToken);
  }

  List<String> MenuItems = ["¿Quiénes somos?"];
  _actionMenu(String action){
    switch(action){
      case "¿Quiénes somos?":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => new AboutScreen()),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        leading: new Image.asset("images/logo.png"),
        actions: <Widget>[
          new PopupMenuButton(itemBuilder: (BuildContext context) {
            return MenuItems.map((String title){
              return new PopupMenuItem(
                  value: title,
                  child: new ListTile(title: Text(title))
              );
            }).toList();
          },
              onSelected: _actionMenu),
          // new IconButton(icon: new Icon(Icons.menu), onPressed: null),
        ],
        elevation: 0.5,
      ),
      body: new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: new Row(
                children: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Row(
                      children: <Widget>[
                        new Icon(Icons.arrow_back),
                        new TitleBox(
                          title: "ATRAS",
                          fontSize: 18.0,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                children: <Widget>[
                  new Container(
                    child: new Expanded(
                      child: new Column(
                        children: <Widget>[
                          new Flex(
                            direction: Axis.horizontal,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new CachedNetworkImage(
                                width: 200.0,
                                imageUrl: productStore.product.picture,
                                fit: BoxFit.cover,
                                placeholder: new Padding(
                                  padding: EdgeInsets.all(35.0),
                                  child: new CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            categoryStore.category.hexColor),
                                  ),
                                ),
                              ),
                              new Expanded(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new TitleBox(
                                      title: productStore.product.product,
                                      fontWeight: FontWeight.bold,
                                      color: new Color(0xFF808080),
                                    ),
                                    new Text(
                                      productStore.product.description,
                                      softWrap: true,
                                      style: new TextStyle(
                                        color:
                                            Color.fromRGBO(101, 100, 106, 1.0),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          new Flexible(
                              child: new Container(
                            padding: EdgeInsets.all(10.0),
                            child: new ListView.builder(
                              itemCount: attributeStore.attributes.length,
                              itemBuilder: (context, index) {
                                return new SectionAttribute(
                                    attribute: attributeStore.attributes[index],
                                    category: categoryStore.category);
                              },
                            ),
                          ))
                        ],
                      ),
                    ),
                  ),
                  new Container(
                    color: categoryStore.category.hexColor,
                    width: 30.0,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SectionAttribute extends StatelessWidget {
  SectionAttribute({
    Key key,
    this.attribute,
    this.category,
  }) : super(key: key);
  final Attribute attribute;
  final Category category;

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          (attribute.title != null && attribute.title != "")
              ? new Padding(
                  padding: EdgeInsets.only(top: 15.0),
                  child: new TitleBox(
                    title: attribute.title.toUpperCase(),
                    color: new Color(0xFF808080),
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                )
              : new Padding(padding: EdgeInsets.only()),
          new Padding(
            padding: EdgeInsets.only(left: 5.0),
            child: new Text(
              attribute.description,
              softWrap: true,
            ),
          )
        ],
      ),
    );
  }
}
