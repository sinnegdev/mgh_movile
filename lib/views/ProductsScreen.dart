import 'package:flutter/material.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/components/ProductList.dart';
import 'package:mgh/views/AboutScreen.dart';

class ProductsScreen extends StatefulWidget {
  @override
  ProductsScreenState createState() => new ProductsScreenState();
}

class ProductsScreenState extends State<ProductsScreen>
    with StoreWatcherMixin<ProductsScreen> {
  @override
  void initState() {
    super.initState();
  }

  List<String> MenuItems = ["¿Quiénes somos?"];
  _actionMenu(String action){
    switch(action){
      case "¿Quiénes somos?":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => new AboutScreen()),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        leading: new Image.asset("images/logo.png"),
        actions: <Widget>[
          new PopupMenuButton(itemBuilder: (BuildContext context) {
            return MenuItems.map((String title){
              return new PopupMenuItem(
                  value: title,
                  child: new ListTile(title: Text(title))
              );
            }).toList();
          },
              onSelected: _actionMenu),
          // new IconButton(icon: new Icon(Icons.menu), onPressed: null),
        ],
        elevation: 0.5,
      ),
      body: new Container(
        child: new CustomScrollView(
          slivers: <Widget>[
            new SliverPersistentHeader(
              delegate: ProductCategoryBanner(
                collapsedHeight: 256.0,
                expandedHeight: 256.0,
              ),
            ),
            new ProductList(),
          ],
        ),
      ),
    );
  }
}
