import 'package:flutter/material.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/components/Carousel.dart';
import 'package:mgh/components/Categories.dart';
import 'package:mgh/stores/BillboardStore.dart';
import 'package:mgh/views/AboutScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen>
    with StoreWatcherMixin<HomeScreen> {
  BillboardStore store;
  @override
  void initState() {
    super.initState();
    store = listenToStore(billboardStoreToken);
  }

  List<String> MenuItems = ["¿Quiénes somos?"];
  _actionMenu(String action){
    switch(action){
      case "¿Quiénes somos?":
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => new AboutScreen()),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        leading: new Image.asset("images/logo.png"),
        actions: <Widget>[
          new PopupMenuButton(itemBuilder: (BuildContext context) {
            return MenuItems.map((String title){
              return new PopupMenuItem(
                  value: title,
                  child: new ListTile(title: Text(title))
              );
            }).toList();
          },
          onSelected: _actionMenu),
          // new IconButton(icon: new Icon(Icons.menu), onPressed: null),
        ],
        elevation: 0.5,
      ),
      body: new Container(
        child: new CustomScrollView(
          slivers: <Widget>[
            new SliverPersistentHeader(
              delegate: SliverCarousel(
                collapsedHeight: 256.0,
                expandedHeight: 256.0,
              ),
            ),
            new Categories(),
          ],
        ),
      ),
    );
  }
}
