import 'package:flutter/material.dart';
import 'package:mgh/views/SplashScreen.dart';
import 'package:mgh/views/HomeScreen.dart';


void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'MH',
      home: new SplashScreen(),
      routes: <String, WidgetBuilder> {
        'HomeScreen' : (BuildContext context) => new HomeScreen(),
      },
      theme: ThemeData(
        primaryColor: Colors.white,
        accentColor: Colors.grey,
        backgroundColor: Colors.white,
      ),
    );
  }
}
