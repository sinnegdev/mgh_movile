import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mgh/models/Category.dart';
import 'package:mgh/stores/CategoryStore.dart';
import 'package:mgh/stores/ProductStore.dart';
import 'package:mgh/views/ProductsScreen.dart';

class Categories extends StatefulWidget {
  @override
  CategoriesState createState() => CategoriesState();
}

class CategoriesState extends State<Categories>
    with StoreWatcherMixin<Categories> {
  CategoryStore categoryStore;

  @override
  void initState() {
    super.initState();
    categoryStore = listenToStore(categoryStoreToken);
  }

  @override
  Widget build(BuildContext context) {
    return new SliverStaggeredGrid.count(
      crossAxisCount: 4,
      staggeredTiles: categoryStore.categories.map((Category category) {
        return StaggeredTile.count(category.columns, category.rows);
      }).toList(),
      children: categoryStore.categories.map((Category category) {
        return CardLayout(category: category);
      }).toList(),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }
}

class CardLayout extends StatelessWidget {
  CardLayout({
    Key key,
    this.category,
  }) : super(key: key);

  final Category category;

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new GestureDetector(
        onTap: () {
          findProductByCategory.call(category.id);
          findCategoryById.call(category.id);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ProductsScreen()),
          );
        },
        child: new CachedNetworkImage(
          imageUrl: category.picture,
          fit: BoxFit.fill,
          placeholder: new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(category.hexColor),
                ),
              ],
            ),
          )
        ),
      ),
      elevation: 0.0,
      shape: BeveledRectangleBorder(borderRadius: BorderRadius.circular(0.0)),
    );
  }
}
