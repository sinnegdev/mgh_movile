import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mgh/components/CategoryBanner.dart';
import 'package:mgh/components/TitleBox.dart';
import 'package:mgh/models/Category.dart';
import 'package:mgh/models/Product.dart';
import 'package:mgh/stores/AttributeStore.dart';
import 'package:mgh/stores/CategoryStore.dart';
import 'package:mgh/stores/ProductStore.dart';
import 'package:mgh/views/ProductDetailScreen.dart';

class ProductList extends StatefulWidget {
  @override
  ProductListState createState() => ProductListState();
}

class ProductListState extends State<ProductList>
    with StoreWatcherMixin<ProductList> {
  ProductStore productStore;
  CategoryStore categoryStore;
  AttributeStore attributeStore;

  @override
  void initState() {
    super.initState();
    productStore = listenToStore(productStoreToken);
    categoryStore = listenToStore(categoryStoreToken);
    attributeStore = listenToStore(attributeStoreToken);
  }

  @override
  Widget build(BuildContext context) {
    return new SliverStaggeredGrid.count(
      crossAxisCount: 6,
      staggeredTiles: productStore.products.map((Product product) {
        return StaggeredTile.count(2, 3);
      }).toList(),
      children: productStore.products.map((Product product) {
        return CardLayout(product: product, category: categoryStore.category);
      }).toList(),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }
}

class CardLayout extends StatelessWidget {
  CardLayout({
    Key key,
    this.product,
    this.category,
  }) : super(key: key);

  final Product product;
  final Category category;

  @override
  Widget build(BuildContext context) {
    return new Card(
      color: Colors.transparent,
      child: new GestureDetector(
        onTap: () {
          findProductById.call(product.id);
          findAttributeByProductId.call(product.id);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => new ProductDetailScreen()),
          );
        },
        child: new Column(
          children: <Widget>[
            new CachedNetworkImage(
              imageUrl: product.picture,
              fit: BoxFit.cover,
              placeholder: new Container(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(category.hexColor),
                    ),
                  ],
                ),
              ),
            ),
            new Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0),
              child: Center(
                child: new TitleBox(
                  title: product.product,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  color: new Color(0xFF808080),
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
      elevation: 0.0,
      shape: BeveledRectangleBorder(borderRadius: BorderRadius.circular(0.0)),
    );
  }
}

class ProductCategoryBanner extends SliverPersistentHeaderDelegate {
  ProductCategoryBanner({this.expandedHeight, this.collapsedHeight});

  final double expandedHeight;
  final double collapsedHeight;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        new Positioned.fill(
            child: new Container(
          margin: EdgeInsets.only(top: 35.0),
          child: new CategoryBanner(),
        )),
        new Container(
          margin: EdgeInsets.symmetric(vertical: 5.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new TitleBox(title: 'PREVENCIÓN ES SALUD'),
            ],
          ),
        )
      ],
    );
  }

  @override
  double get minExtent => collapsedHeight;

  @override
  double get maxExtent => math.max(expandedHeight, minExtent);

  @override
  bool shouldRebuild(ProductCategoryBanner oldDelegate) {
    return expandedHeight != oldDelegate.expandedHeight ||
        collapsedHeight != oldDelegate.collapsedHeight;
  }
}
