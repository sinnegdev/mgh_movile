import 'dart:math' as math;

import 'package:flutter/material.dart';

class TitleBox extends StatelessWidget {
  const TitleBox({
    Key key,
    this.title: "Titulo",
    this.fontSize: 24.0,
    this.fontWeight: FontWeight.w400,
    this.color: const Color.fromRGBO(101, 100, 106, 1.0),
    this.textAlign: TextAlign.left,
  }) : super(key: key);

  final title;
  final fontSize;
  final fontWeight;
  final textAlign;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return new Text(
      title,
      textAlign: textAlign,
      style: DefaultTextStyle.of(context).style.merge(new TextStyle(
            color: color,
            fontSize: fontSize,
            fontFamily: 'Roboto',
            fontWeight: fontWeight,
          )),
      softWrap: true,
    );
  }
}

class SliverTitleBox extends SliverPersistentHeaderDelegate {
  SliverTitleBox({
    this.expandedHeight,
    this.collapsedHeight,
    this.title: "Titulo",
    this.fontSize: 24.0,
    this.fontWeight: FontWeight.w400,
  });

  final double expandedHeight;
  final double collapsedHeight;

  final title;
  final fontSize;
  final fontWeight;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new TitleBox(
      title: title,
      fontSize: fontSize,
      fontWeight: fontWeight,
    );
  }

  @override
  double get minExtent => collapsedHeight;
  @override
  double get maxExtent => math.max(expandedHeight, minExtent);

  @override
  bool shouldRebuild(SliverTitleBox oldDelegate) {
    return expandedHeight != oldDelegate.expandedHeight ||
        collapsedHeight != oldDelegate.collapsedHeight;
  }
}
