import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/components/TitleBox.dart';
import 'package:mgh/models/Billboard.dart';
import 'package:mgh/stores/BillboardStore.dart';

class CarouselPanel extends StatefulWidget {
  @override
  CarouselState createState() => new CarouselState();
}

class CarouselState extends State<CarouselPanel>
    with StoreWatcherMixin<CarouselPanel> {
  BillboardStore store;

  @override
  void initState() {
    super.initState();
    store = listenToStore(billboardStoreToken);
  }

  @override
  Widget build(BuildContext context) {
    return new CarouselSlider(
      items: store.billboards.map((Billboard billboard) {
        return new Container(
            margin: new EdgeInsets.all(2.0),
            child: new ClipRRect(
                borderRadius: new BorderRadius.all(new Radius.circular(0.0)),
                child: CachedNetworkImage(
                  imageUrl: billboard.picture,
                  fit: BoxFit.fill,
                )));
      }).toList(),
      interval: Duration(seconds: 8),
      viewportFraction: 0.95,
      aspectRatio: 1.5,
      autoPlay: true,
      height: 208.0,
    );
  }
}

class SliverCarousel extends SliverPersistentHeaderDelegate {
  SliverCarousel({
    this.expandedHeight,
    this.collapsedHeight,
  });

  final double expandedHeight;
  final double collapsedHeight;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: new TitleBox(title: 'PREVENCIÓN ES SALUD'),
        ),
        new CarouselPanel(),
      ],
    );
  }

  @override
  double get minExtent => collapsedHeight;

  @override
  double get maxExtent => math.max(expandedHeight, minExtent);

  @override
  bool shouldRebuild(SliverCarousel oldDelegate) {
    return expandedHeight != oldDelegate.expandedHeight ||
        collapsedHeight != oldDelegate.collapsedHeight;
  }
}
