import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flux/flutter_flux.dart';
import 'package:mgh/stores/CategoryStore.dart';

class CategoryBanner extends StatefulWidget {
  @override
  CategoryBannerState createState() => CategoryBannerState();
}

class CategoryBannerState extends State<CategoryBanner>
    with StoreWatcherMixin<CategoryBanner> {
  CategoryStore categoryStore;

  @override
  void initState() {
    super.initState();
    categoryStore = listenToStore(categoryStoreToken);
  }

  @override
  Widget build(BuildContext context) {
    return new CachedNetworkImage(
      imageUrl: categoryStore.category.banner,
      fit: BoxFit.fill,
      height: 208.0,
      placeholder: new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(categoryStore.category.hexColor),
            ),
          ],
        ),
      ),
    );
  }
}
